# media-player

##快速开始


```
package main

import (
	player "gitee.com/junbinyang/media-player"
	"container/list"
        "fmt"
)

func main(){
	files := list.New()
	files.PushBack("file:///mnt/d/dev/go/src/media-player/2.mp4")
	s := player.NewMediaPlayer(files)
	s.Run()
}
```

##其他操作


```
package main

import (
	player "gitee.com/junbinyang/media-player"
	"container/list"
""
)

func main(){
	files := list.New()
	files.PushBack("file:///mnt/d/dev/go/src/media-player/2.mp4")
        files.PushBack("https://www.runoob.com/try/demo_source/mov_bbb.mp4")

        // 设置回调
        player.StateChangedCallBack = func(name string) {
		fmt.Println("State changed -> "+name)
	}
        // ... ErrorCallBack | BufferingCallBack | EOSCallBack | StateChangedCallBack
        // 总线错误 | 加载资源 | 播放结束 | 状态变更

	s := player.NewMediaPlayer(files)
	go s.Run()
    
        s.ToggleStatus()    // 切换播放状态
        //s.Play()            
        //s.Pause()
        //s.SetVolume()       
        //s.SetSeek()          
        //s.SetRate()
        select{}
}
```
