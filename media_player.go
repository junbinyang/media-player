package media_player

/*
#cgo pkg-config: gstreamer-1.0 gstreamer-player-1.0
#include <gst/gst.h>
#include <gst/player/player.h>
#include <stdio.h>

typedef struct player_
{
  GstPlayer *player;
  GstState desired_state;
  GMainLoop *loop;
} Player;

void go_error_cb(gchar* message);
void go_state_changed_cb(void* name);
void go_buffering_cb(Player *play, gint *percent);
void go_end_of_stream_cb(Player *play);

static void play_free (Player * play)
{
  gst_object_unref (play->player);
  g_main_loop_unref (play->loop);
  g_free (play);
  gst_deinit();
}

static void loop_quit(Player *play)
{
	g_main_loop_quit (play->loop);
}

static void error_cb (GstPlayer * player, GError * err, Player * play)
{
  go_error_cb(err->message);
  loop_quit(play);
}

static void state_changed_cb (GstPlayer * player, GstPlayerState state, Player * play)
{
  go_state_changed_cb((void *)(gst_player_state_get_name (state)));
}

static void buffering_cb (GstPlayer * player, gint percent, Player * play)
{
	printf("%d",percent);
  	go_buffering_cb(play, &percent);
}

static void end_of_stream_cb (GstPlayer * player, Player * play)
{
  	go_end_of_stream_cb(play);
}

static Player *new_player() 
{
	Player *play;
	play = g_new0 (Player, 1);
	play->player = gst_player_new(NULL, gst_player_g_main_context_signal_dispatcher_new(NULL));

	g_signal_connect (play->player, "error", G_CALLBACK (error_cb), play);
	g_signal_connect (play->player, "state-changed", G_CALLBACK (state_changed_cb), play);
	g_signal_connect (play->player, "buffering", G_CALLBACK (buffering_cb), play);
	g_signal_connect (play->player, "end-of-stream", G_CALLBACK (end_of_stream_cb), play);

	play->loop = g_main_loop_new (NULL, FALSE);

	return play;
}

static void set_uri(Player *play, gchar *uri) 
{
    gst_player_set_uri (play->player, uri);
}

static void init_status(Player *play, gboolean autoplay) 
{
	if (!autoplay) {
		play->desired_state = GST_STATE_PAUSED;
    	gst_player_pause (play->player);
	} else {
		play->desired_state = GST_STATE_PLAYING;
  		gst_player_play (play->player);
	}    
}

static void run(Player *play) 
{
	g_main_loop_run (play->loop);
}

static void toggle_paused (Player * play)
{
  if (play->desired_state == GST_STATE_PLAYING) {
    play->desired_state = GST_STATE_PAUSED;
    gst_player_pause (play->player);
  } else {
    play->desired_state = GST_STATE_PLAYING;
    gst_player_play (play->player);
  }
}

static void pause (Player * play)
{
    play->desired_state = GST_STATE_PAUSED;
    gst_player_pause (play->player);
}

static void play (Player * play)
{
    play->desired_state = GST_STATE_PLAYING;
    gst_player_play (play->player);
}

static void set_volume (Player * play, gdouble volume_step)
{
  	g_object_set (play->player, "volume", volume_step, NULL);
}

static void set_rate (Player * play, gdouble rate_step)
{
  	gst_player_set_rate(play->player, rate_step);
}

static void relative_seek (Player * play, gdouble percent)
{
  gint64 dur = -1, pos = -1;

  g_return_if_fail (percent >= -1.0 && percent <= 1.0);

  g_object_get (play->player, "position", &pos, "duration", &dur, NULL);

  if (dur <= 0) {
    //g_print ("\nCould not seek.\n");
    return;
  }

  pos = pos + dur * percent;
  if (pos < 0)
    pos = 0;
  gst_player_seek (play->player, pos);
}
*/
import "C"
import (
	"fmt"
	"container/list"
	"unsafe"
	"errors"
)

type MediaOption struct {
	Volume 		float64
	Rate 		float64
	AutoPlay 	bool
}

type MediaPlayer struct {
	play *C.struct_player_
	playlist *list.List
	option MediaOption
}

func (this *MediaPlayer) Run() {
	for e := this.playlist.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value.(string))
		play_source := Gstring(e.Value.(string))
		C.set_uri(this.play, play_source)
		Gfree(unsafe.Pointer(play_source))

		if this.option.AutoPlay {
			C.init_status(this.play, C.gboolean(1))
		} else {
			C.init_status(this.play, C.gboolean(0))
		}

		C.run(this.play)
	}

	//C.run(this.play)
	C.play_free(this.play)
}

func (this *MediaPlayer) ToggleStatus() {
	C.toggle_paused(this.play)
}

func (this *MediaPlayer) Play() {
	C.play(this.play)
}

func (this *MediaPlayer) Pause() {
	C.pause(this.play)
}

func (this *MediaPlayer) SetVolume(volume_step float64) {
	volume := *(*C.gdouble)(unsafe.Pointer(&volume_step))
	C.set_volume(this.play, volume)
}

func (this *MediaPlayer) SetSeek(percent_step float64) {
	percent := *(*C.gdouble)(unsafe.Pointer(&percent_step))
	C.relative_seek(this.play, percent)
}

func (this *MediaPlayer) SetRate(rate_step float64) {
	rate := *(*C.gdouble)(unsafe.Pointer(&rate_step))
	C.set_rate(this.play, rate)
}

func NewMediaPlayer (playlist *list.List, option ...MediaOption) MediaPlayer {
	res := MediaPlayer{}
	res.play = C.new_player()
	res.playlist = playlist
	if len(option) > 0 {
		res.option = option[0]
	} else {
		res.option = MediaOption{5.0, 1.0, true}
	}
	res.SetVolume(res.option.Volume)
	res.SetRate(res.option.Rate)

	return res
}

func Gstring(s string) *C.gchar {
    return (*C.gchar)(C.CString(s))
}

func Gfree(s unsafe.Pointer) {
    C.g_free(C.gpointer(s))
}

var ErrorCallBack func(error)
//export go_error_cb
func go_error_cb(msg *C.gchar) {
	str := C.GoString((*C.char)(unsafe.Pointer(msg)))
	if ErrorCallBack != nil {
		ErrorCallBack(errors.New(str))
	}
}

var StateChangedCallBack func(string)
//export go_state_changed_cb
func go_state_changed_cb(name unsafe.Pointer) {
	str := C.GoString((*C.char)(name))
	if StateChangedCallBack != nil {
		StateChangedCallBack(str)
	}
}

var BufferingCallBack func(int64) = func(percent int64) {
	fmt.Printf("Buffering percent: %d\n", percent)
}
//export go_buffering_cb
func go_buffering_cb(play *C.Player, percent *C.gint) {
	per := *(*int64)(unsafe.Pointer(percent))
	if per < 100 {
		C.pause(play)
	}else{
		C.play(play)
	}
	BufferingCallBack(per)
}

var EOSCallBack func() = func(){
	fmt.Println("End of the play")
}
//export go_end_of_stream_cb
func go_end_of_stream_cb(play *C.Player) {
	if EOSCallBack != nil {
		EOSCallBack()
	}
	C.loop_quit(play)
}